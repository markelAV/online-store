create table public.type_product
(
    id serial not null
        constraint type_product_pk
            primary key,
    name varchar not null,
    description varchar
);

create table public.category_product
(
    id serial not null
        constraint category_product_pk
            primary key,
    name varchar not null,
    description varchar,
    id_type_product int
        constraint category_product_type_product_id_fk
            references type_product
            on update cascade on delete cascade
);
create table public.product
(
    id serial not null
        constraint product_pk
            primary key,
    name varchar not null,
    photo varchar,
    description varchar,
    price float4 default 0.0 not null,
    rating float4 default 0.0 not null,
    id_category_product int not null
        constraint product_category_product_id_fk
            references category_product
            on update cascade on delete cascade
);

create table public.store
(
    id serial not null
        constraint store_pk
            primary key,
    address varchar not null,
    working_hours varchar not null
);

create table public.type_user
(
    id serial not null
        constraint type_user_pk
            primary key,
    name varchar not null,
    description varchar
);

create unique index type_user_name_uindex
    on type_user (name);

create table public.users
(
    id serial not null
        constraint user_pk
            primary key,
    login varchar not null,
    password varchar not null,
    name         varchar not null,
    id_type_user integer
        constraint user_type_user_id_fk
            references type_user
            on update cascade on delete cascade
);

create unique index user_login_uindex
    on users (login);

create table public.stock
(
    id_store int not null
        constraint stock_store_id_fk
            references store
            on update cascade on delete cascade,
    id_product int not null
        constraint stock_product_id_fk
            references product
            on update cascade on delete cascade,
    quantity int default 0 not null,
    constraint stock_pk
        primary key (id_store, id_product)
);

create table public.workers
(
    id_user int not null
        constraint workers_user_id_fk
            references users
            on update cascade on delete cascade,
    id_store int not null
        constraint workers_store_id_fk
            references store
            on update cascade on delete cascade,
    constraint workers_pk
        primary key (id_user, id_store)
);

create table public.orders
(
    id serial not null
        constraint orders_pk
            primary key,
    status varchar default 'is created by' not null,
    sum float4 default 0.0 not null,
    id_store int not null
        constraint orders_store_id_fk
            references store
            on update cascade on delete cascade,
    id_user int not null
        constraint orders_user_id_fk
            references users
            on update cascade on delete cascade
);


create table public.products_of_order
(
    id_product int not null
        constraint products_of_order_product_id_fk
            references product
            on update cascade on delete cascade,
    id_order int not null
        constraint products_of_order_orders_id_fk
            references orders
            on update cascade on delete cascade,
    count int default 1 not null,
    constraint products_of_order_pk
        primary key (id_product, id_order)
);

--inserts

--users

INSERT INTO public.store (id, address, working_hours) VALUES (1, 'Samara', '8:00 - 20:00');

INSERT INTO public.type_user (id, name, description) VALUES (1, 'Admin', 'all access');
INSERT INTO public.type_user (id, name, description) VALUES (2, 'Manager', null);
INSERT INTO public.type_user (id, name, description) VALUES (3, 'Customer', null);

INSERT INTO public.users (id, login, password, name, id_type_user) VALUES (1, 'admin', 'admin', 'UserAdmin', 1);
INSERT INTO public.users (id, login, password, name, id_type_user) VALUES (2, 'manager', 'manager', 'ManagerUser', 2);
INSERT INTO public.users (id, login, password, name, id_type_user) VALUES (3, 'customer', 'customer', 'CustomerUser', 3);

INSERT INTO public.workers (id_user, id_store) VALUES (2, 1);

--products

INSERT INTO public.type_product (id, name, description) VALUES (1, 'Гитары', 'Описание этого вида игструмента');
INSERT INTO public.type_product (id, name, description) VALUES (2, 'Клавишные инструменты', null);

INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (1, 'Классические гитары', null, 1);
INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (2, 'Акустические гитары', null, 1);
INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (3, 'Электрогитары', null, 1);
INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (4, 'Бас-гитары', null, 1);
INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (5, 'Синтезаторы', null, 2);
INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (6, 'Цифровые пианино', null, 2);
INSERT INTO public.category_product (id, name, description, id_type_product) VALUES (7, 'MIDI-клавиатуры', null, 2);

INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (1, 'YAMAHA C40', 'images_for_products/yamahaf310.png', 'Хоршая бюджетная аккутическая гитара', 7999, 0, 1);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (2, 'PRADO HS - 3805 / BK', 'images_for_products/yamahaf310.png', null, 2575, 0, 1);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (3, 'ROCKDALE MODERN CLASSIC 100-N', 'images_for_products/yamahaf310.png', null, 4700, 0, 1);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (5, 'YAMAHA F310', 'images_for_products/yamahaf310.png', null, 9990, 0, 2);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (6, 'FENDER SQUIER SA-150 DREADNOUGHT, NAT', 'images_for_products/yamahaf310.png', null, 6700, 0, 2);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (7, 'VESTON F-38/BK', 'images_for_products/yamahaf310.png', null, 5200, 0, 2);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (8, 'FENDER SQUIER MM STRATOCASTER HARD TAIL', 'images_for_products/yamahaf310.png', null, 8700, 0, 3);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (9, 'YAMAHA PACIFICA 012 BL', 'images_for_products/yamahaf310.png', null, 15990, 0, 3);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (10, 'IBANEZ GRX20-BKN BLACK', 'images_for_products/yamahaf310.png', null, 15500, 0, 3);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (11, 'IBANEZ GIO GSR200B-WNF WALNUT FLAT', 'images_for_products/yamahaf310.png', null, 20000, 0, 4);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (12, 'EPIPHONE THUNDERBIRD IV BASS REVERSE VINTAGE', 'images_for_products/yamahaf310.png', null, 36000, 0, 4);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (13, 'YAMAHA PSR-E263', 'images_for_products/yamahaf310.png', null, 11490, 0, 5);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (14, 'CASIO CTK-1500', 'images_for_products/yamahaf310.png', null, 7990, 0, 5);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (15, 'CASIO CDP-S100BK', 'images_for_products/yamahaf310.png', null, 29990, 0, 6);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (16, 'IK MULTIMEDIA IRIG KEYS 25', 'images_for_products/yamahaf310.png', null, 4490, 0, 7);
INSERT INTO public.product (id, name, photo, description, price, rating, id_category_product) VALUES (17, 'ROLAND A-49-BK', 'images_for_products/yamahaf310.png', null, 11601, 0, 7);

--orders

INSERT INTO public.orders (id, status, sum, id_store, id_user) VALUES (1, 'open', 2000.0, 1, 3);

INSERT INTO public.stock (id_store, id_product, quantity) VALUES (1, 1, 3);
INSERT INTO public.stock (id_store, id_product, quantity) VALUES (1, 2, 2);
INSERT INTO public.stock (id_store, id_product, quantity) VALUES (1, 3, 1);
INSERT INTO public.stock (id_store, id_product, quantity) VALUES (1, 5, 0);
