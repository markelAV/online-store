package com.store.servlet;

import com.store.model.UsersEntity;
import com.store.service.user.UserService;
import com.store.service.user.UserServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListUsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = new UserServiceImpl();
        List<UsersEntity> users = userService.getAllUser();
        req.setAttribute("users", users);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/list_users.jsp");
        requestDispatcher.forward(req, resp);

    }
}
