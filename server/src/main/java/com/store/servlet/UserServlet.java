package com.store.servlet;

import com.store.model.UsersEntity;
import com.store.service.user.UserService;
import com.store.service.user.UserServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        UserService userService = new UserServiceImpl();
        UsersEntity user = userService.getUserById(id);
        req.setAttribute("user", user);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/user.jsp");
        requestDispatcher.forward(req, resp);
    }
}
