package com.store.servlet;

import com.store.service.order.OrderService;
import com.store.service.order.OrderServiceImpl;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.service.store.StoreService;
import com.store.service.store.StoreServiceImpl;
import com.store.service.user.UserService;
import com.store.service.user.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteEntityServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String typeEntity = req.getParameter("entity");
        String id = req.getParameter("id");

        if (typeEntity != null && id != null) {
            ProductService productService = new ProductServiceImpl();
            StoreService storeService = new StoreServiceImpl();
            UserService userService = new UserServiceImpl();
            OrderService orderService = new OrderServiceImpl();

            switch (typeEntity) {
                case "type":
                    productService.deleteTypeById(Integer.parseInt(id));
                    resp.sendRedirect("/server_war_exploded/types");
                    break;
                case "category":
                    productService.deleteCategoryById(Integer.parseInt(id));
                    resp.sendRedirect("/server_war_exploded/categories");
                    break;
                case "product":
                    productService.deleteProductById(Integer.parseInt(id));
                    resp.sendRedirect("/server_war_exploded/products");
                    break;
                case "store":
                    storeService.deleteStore(Integer.parseInt(id));
                    resp.sendRedirect("/server_war_exploded/stores");
                    break;
                case "user":
                    userService.deleteUser(Integer.parseInt(id));
                    resp.sendRedirect("/server_war_exploded/users");
                    break;
                case "order":
                    orderService.deleteOrder(Integer.parseInt(id));
                    resp.sendRedirect("/server_war_exploded/orders");
                    break;
            }
        }
    }
}
