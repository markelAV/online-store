package com.store.servlet;

import com.store.model.*;
import com.store.service.order.OrderService;
import com.store.service.order.OrderServiceImpl;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.service.store.StoreService;
import com.store.service.store.StoreServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ShoppingCardServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idStore = req.getParameter("store");
        if (idStore != null) {
            OrderService orderService = new OrderServiceImpl();
            OrdersEntity order = new OrdersEntity();

            Cookie user = getCookie(req, "userId");
            String idUser = user != null ? user.getValue() : "1"; // Todo select id = 1 for simple development(without authorization)
            int orderId = generateId(idUser);
            float sum = Float.parseFloat(req.getParameter("sum"));
            order.setId(orderId);
            order.setStatus("created");
            order.setSum(sum);
            order.setStore(new StoreEntity(Integer.parseInt(idStore)));
            order.setUser(new UsersEntity(Integer.parseInt(idUser)));
            orderService.addOrder(order);

            HttpSession session = req.getSession();
            HashMap<String, Integer> products = (HashMap<String, Integer>) session.getAttribute("ids");
            if (products != null && products.size() > 0) {
                Set<String> idsProducts = products.keySet();
                for (String id : idsProducts) {
                    orderService.addProductInOrder(new ProductsOfOrderEntity(orderId, Integer.parseInt(id), products.get(id)));
                }

                session.removeAttribute("ids");
            }
        }
        resp.sendRedirect("/server_war_exploded/orders");
    }

    private int generateId(String idUser) {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("mm");
        return Integer.parseInt(idUser + format.format(date));
    }

    private Cookie getCookie(HttpServletRequest request, String key) {
        Cookie[] cookies = request.getCookies();
        Cookie cookie = null;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (key.equals(c.getName())) {
                    cookie = c;
                    break;
                }
            }
        }
        return cookie;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        HashMap<String, Integer> products = (HashMap<String, Integer>) session.getAttribute("ids");
        List<ProductEntity> productEntities = null;
        if (products != null) {

            Set<String> ids = products.keySet();
            ProductService productService = new ProductServiceImpl();
            StoreService storeService = new StoreServiceImpl();

            productEntities = new LinkedList<>();
            ProductEntity temp = null;
            for (String id : ids) {
                temp = productService.findProductById(Integer.parseInt(id));
                if (temp != null) {
                    productEntities.add(temp);
                }
            }

            req.setAttribute("productsE", productEntities);
            req.setAttribute("productsQ", products);
            req.setAttribute("stores", storeService.getAllStores());
            req.setAttribute("sum", countSumOfOrder(productEntities, products));
            req.setAttribute("countOrder", String.valueOf(productEntities.size()));
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/shopping_card.jsp");
        requestDispatcher.forward(req, resp);
    }

    private float countSumOfOrder(List<ProductEntity> products, HashMap<String, Integer> productsQ) {
        float sum = 0.0f;
        if (products.size() == productsQ.size()) {
            for (ProductEntity product : products) {
                String id = String.valueOf(product.getId());
                if (productsQ.get(id) != null) {
                    sum += product.getPrice() * productsQ.get(id);
                }
            }
        }
        return sum;
    }
}
