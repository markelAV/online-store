package com.store.servlet;

import com.store.model.ProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CatalogProductsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id != null) {
            ProductService productService = new ProductServiceImpl();
            List<ProductEntity> products = productService.searchProductsByCategory(Integer.parseInt(id));
            req.setAttribute("products", products);
            req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/catalog_products.jsp");
            requestDispatcher.forward(req, resp);
        }
    }
}
