package com.store.servlet;

import com.store.model.OrdersEntity;
import com.store.model.ProductEntity;
import com.store.model.ProductsOfOrderEntity;
import com.store.service.order.OrderService;
import com.store.service.order.OrderServiceImpl;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class OrderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        OrderService orderService = new OrderServiceImpl();
        ProductService productService = new ProductServiceImpl();
        OrdersEntity order = orderService.getOrderById(id);
        List<ProductsOfOrderEntity> productsOrder = orderService.getProductsOfOrderById(id);
        List<Integer> idsProducts = new LinkedList<>();
        for (ProductsOfOrderEntity productOrder : productsOrder) {
            idsProducts.add(productOrder.getProductIdOrder().getId_product());
        }
        List<ProductEntity> products = productService.findProductByIds(idsProducts);
        req.setAttribute("order", order);
        req.setAttribute("products", products);
        req.setAttribute("productsOrder", productsOrder);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/order.jsp");
        requestDispatcher.forward(req, resp);
    }
}
