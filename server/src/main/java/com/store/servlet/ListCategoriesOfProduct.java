package com.store.servlet;

import com.store.model.CategoryProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListCategoriesOfProduct extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService productService = new ProductServiceImpl();
        List<CategoryProductEntity> categories = productService.getAllCategoryOfProduct();
        req.setAttribute("categories", categories);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/list_categories_of_product.jsp");
        requestDispatcher.forward(req, resp);
    }
}
