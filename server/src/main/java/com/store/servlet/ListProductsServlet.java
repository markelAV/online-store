package com.store.servlet;

import com.store.model.ProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListProductsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //int id = Integer.parseInt(req.getParameter("id"));
        ProductService productService = new ProductServiceImpl();
        List<ProductEntity> products = productService.getAllProducts();
        req.setAttribute("products", products);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/list_products.jsp");
        requestDispatcher.forward(req, resp);
    }
}
