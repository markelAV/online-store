package com.store.servlet;

import com.store.model.OrdersEntity;
import com.store.service.order.OrderService;
import com.store.service.order.OrderServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListOrdersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OrderService orderService = new OrderServiceImpl();
        List<OrdersEntity> orders = orderService.getAllOrders();
        req.setAttribute("orders", orders);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/list_orders.jsp");
        requestDispatcher.forward(req, resp);
    }
}
