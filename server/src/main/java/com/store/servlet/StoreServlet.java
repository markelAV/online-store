package com.store.servlet;

import com.store.model.StoreEntity;
import com.store.service.store.StoreService;
import com.store.service.store.StoreServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StoreServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        StoreService storeService = new StoreServiceImpl();
        StoreEntity store = storeService.getStoreById(id);
        req.setAttribute("store", store);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/store.jsp");
        requestDispatcher.forward(req, resp);
    }
}
