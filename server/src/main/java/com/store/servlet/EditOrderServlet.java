package com.store.servlet;

import com.store.model.OrdersEntity;
import com.store.model.StoreEntity;
import com.store.model.TypeUserEntity;
import com.store.model.UsersEntity;
import com.store.service.order.OrderService;
import com.store.service.order.OrderServiceImpl;
import com.store.service.store.StoreService;
import com.store.service.store.StoreServiceImpl;
import com.store.service.user.UserService;
import com.store.service.user.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class EditOrderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idOrder = req.getParameter("id");
        if (idOrder != null) {
            OrderService orderService = new OrderServiceImpl();
            UserService userService = new UserServiceImpl();
            StoreService storeService = new StoreServiceImpl();
            OrdersEntity order = orderService.getOrderById(Integer.parseInt(idOrder));
            List<StoreEntity> stores = storeService.getAllStores();
            List<UsersEntity> users = userService.getAllUser();
            req.setAttribute("order", order);
            req.setAttribute("stores", stores);
            req.setAttribute("users", users);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/edit_order.jsp");
            requestDispatcher.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id != null && !id.equals("")) {
            String status = req.getParameter("status");
            Float sum = Float.parseFloat(req.getParameter("sum"));
            int idStore = Integer.parseInt(req.getParameter("idStore"));
            int idUser = Integer.parseInt(req.getParameter("idUser"));
            StoreEntity store = new StoreEntity();
            store.setId(idStore);
            UsersEntity user = new UsersEntity();
            user.setId(idUser);
            OrdersEntity order = new OrdersEntity(Integer.parseInt(id), status, sum, store, user);
            OrderService orderService = new OrderServiceImpl();
            orderService.updateOrder(order);
        }

        resp.sendRedirect("/server_war_exploded/orders");
    }
}
