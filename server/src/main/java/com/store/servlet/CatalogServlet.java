package com.store.servlet;

import com.store.model.CategoryProductEntity;
import com.store.model.ProductEntity;
import com.store.service.product.*;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CatalogServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService productService = new ProductServiceImpl();
        List<CategoryProductEntity> categories = null;
        List<ProductEntity> products = null;

        String type = req.getParameter("type");
        String category = req.getParameter("category");
        String selectType = "";
        if ((type != null && !type.equals("")) || (category != null && !category.equals(""))) {
            if (category != null && !category.equals("")) {
                int idCategory = Integer.parseInt(category);
                products = productService.searchProductsByCategory(idCategory);
                CategoryProductEntity categoryEntity = productService.findCategoryById(idCategory);
                categories = productService.searchCategoriesByType(categoryEntity.getTypeProduct().getId());
                selectType = categoryEntity.getTypeProduct().getName();
            } else {
                categories = productService.searchCategoriesByType(Integer.parseInt(type));
                products = productService.searchProductsByTypeId(Integer.parseInt(type));
                selectType = productService.findTypeById(Integer.parseInt(type)).getName();
            }
        } else {
            categories = productService.getAllCategoryOfProduct();
            products = productService.getAllProducts();
        }

        req.setAttribute("categories", categories);
        req.setAttribute("products", products);
        req.setAttribute("type", selectType);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/catalog_products.jsp");
        requestDispatcher.forward(req, resp);
    }
}
