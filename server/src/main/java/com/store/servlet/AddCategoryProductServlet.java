package com.store.servlet;

import com.store.model.CategoryProductEntity;
import com.store.model.TypeProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AddCategoryProductServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idCategory = req.getParameter("id");
        ProductService productService = new ProductServiceImpl();
        if (idCategory != null) {
            CategoryProductEntity category = productService.findCategoryById(Integer.parseInt(idCategory));
            req.setAttribute("category", category);
        }

        List<TypeProductEntity> types = productService.getAllTypeOfProduct();
        req.setAttribute("types", types);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/create_category_of_product.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("name").length() > 0) {
            String name = req.getParameter("name");
            String id = req.getParameter("id");
            String description = req.getParameter("description");
            int idType = Integer.parseInt(req.getParameter("idType"));
            TypeProductEntity type = new TypeProductEntity(); //Fixme please for only work with category
            type.setId(idType);
            CategoryProductEntity categoryProductEntity = new CategoryProductEntity();
            categoryProductEntity.setName(name);
            categoryProductEntity.setTypeProduct(type);
            if (!description.equals("")) {
                categoryProductEntity.setDescription(description);
            }
            ProductService productService = new ProductServiceImpl();
            if (id != null) {
                categoryProductEntity.setId(Integer.parseInt(id));
                productService.updateCategoryOfProduct(categoryProductEntity);
            } else {
                productService.addCategoryOfProduct(categoryProductEntity);
            }
            resp.sendRedirect("/server_war_exploded/categories");

        }
    }
}
