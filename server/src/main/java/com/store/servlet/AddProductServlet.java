package com.store.servlet;

import com.store.model.CategoryProductEntity;
import com.store.model.ProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.store.utils.ShoppingUtil;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class AddProductServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService productService = new ProductServiceImpl();
        String idProduct = req.getParameter("id");
        if (idProduct != null) {
            ProductEntity product = productService.findProductById(Integer.parseInt(idProduct));
            req.setAttribute("product", product);
        }
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        List<CategoryProductEntity> categories = productService.getAllCategoryOfProduct();
        req.setAttribute("categories", categories);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/create_product.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ServletFileUpload.isMultipartContent(req)) {
            String filePath = null;
            String name = null;
            String description = null;
            int id = -1;
            int idCategory = -1;
            float price = -1.0f;
            /* Работа с загрузкой файла*/
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(1024 * 1024);

            File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
            factory.setRepository(tempDir);

            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(1024 * 1024 * 10);
            try {
                List items = upload.parseRequest(req);
                Iterator iter = items.iterator();

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (item.isFormField()) { // Check what this part loading file is field form
                        switch (item.getFieldName()) {
                            case "name":
                                name = new String(item.get(), "UTF-8");
                                break;
                            case "id":
                                if (!item.getString().equals("")) {
                                    id = Integer.parseInt(item.getString());
                                }
                                break;
                            case "description":
                                description = new String(item.get(), "UTF-8");
                                break;
                            case "idCategory":
                                idCategory = Integer.parseInt(item.getString());
                                break;
                            case "price":
                                price = Float.parseFloat(item.getString());
                                break;
                        }

                    } else {
                        filePath = processUploadedFile(item);
                    }
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
                // add response error
            } catch (Exception e) {
                e.printStackTrace();
                //Fixme add response error
            }

            if (name != null) {
                ProductService service = new ProductServiceImpl();
                CategoryProductEntity categoryProductEntity = new CategoryProductEntity();
                categoryProductEntity.setId(idCategory);
                ProductEntity product = new ProductEntity();
                product.setName(name);
                product.setPrice(price);
                product.setCategoryProduct(categoryProductEntity);
                if (filePath != null && !filePath.equals("")) {
                    product.setPhoto(filePath);
                } else if (id != -1) {
                    ProductEntity productUpdate = service.findProductById(id);
                    if (productUpdate != null) {
                        product.setPhoto(productUpdate.getPhoto());
                    }
                }
                if (!description.equals("")) {
                    product.setDescription(description);
                }
                if (id != -1) {
                    product.setId(id);
                    service.updateProduct(product);
                } else {
                    service.addProduct(product);
                }
            }
        } else {
            System.out.println("Not multipart");
        }
        resp.sendRedirect("/server_war_exploded/products");
    }

    private String processUploadedFile(FileItem item) throws Exception {
        File uploadetFile = null;
        File uploadetFileServer = null;
        String result = "images_for_products/" + item.getName();

        //Fixme please !! use generate uuid for saving files

        String pathServer = getServletContext().getRealPath("/images_for_products/" + item.getName());
        String path = "C:\\Users\\artem\\IdeaProjects\\online-store\\server\\web\\images_for_products/" + item.getName(); // todo переименовать файл на случай инъекций
        uploadetFile = new File(path);
        uploadetFileServer = new File(pathServer);
        if (uploadetFile.exists()) {
            System.out.println("Files exist");
        } else {
            uploadetFile.createNewFile();
            item.write(uploadetFile);
        }
        if (uploadetFileServer.exists()) {
            System.out.println("Files exist");
        } else {
            uploadetFileServer.createNewFile();
            item.write(uploadetFileServer);
        }
        return result;
    }
}
