package com.store.servlet;

import com.store.utils.ShoppingUtil;
import com.sun.istack.NotNull;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class AddProductInShoppingCardServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String id = req.getParameter("id");
        String operation = req.getParameter("operation");
        String path = req.getHeader("referer");

        if (id != null && operation != null && (operation.equals("inc") || operation.equals("dec"))) {
            HashMap<String, Integer> shopCard = (HashMap<String, Integer>) session.getAttribute("ids");
            shopCard = editShoppingCard(shopCard, operation, id);
            session.setAttribute("ids", shopCard);
        }
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        resp.sendRedirect(path);
    }

    private HashMap<String, Integer> editShoppingCard(HashMap<String, Integer> products, @NotNull String operation, @NotNull String id) {
        if (operation.equals("inc")) {
            products = products != null ? products : new HashMap<String, Integer>();
            int quantity = products.get(id) == null ? 1 : products.get(id) + 1;
            products.put(id, quantity);
        } else {
            if (products != null && products.get(id) != null) {
                int quantity = products.get(id);
                quantity--;
                if (quantity > 0) {
                    products.put(id, quantity);
                } else {
                    products.remove(id);
                }
            }
            //Todo maybe add logic in block else for show error user
        }
        return products;
    }

}
