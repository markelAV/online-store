package com.store.servlet;

import com.store.model.StoreEntity;
import com.store.service.store.StoreService;
import com.store.service.store.StoreServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddStoreServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id != null) {
            StoreService service = new StoreServiceImpl();
            StoreEntity store = service.getStoreById(Integer.parseInt(id));
            req.setAttribute("store", store);
        }
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/create_store.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("address").length() > 0) {
            String address = req.getParameter("address");
            String workingHours = req.getParameter("working_hours");
            String id = req.getParameter("id");
            StoreService storeService = new StoreServiceImpl();
            StoreEntity store = new StoreEntity(0, address, workingHours);
            if (id != null) {
                store.setId(Integer.parseInt(id));
                storeService.updateStore(store);
            } else {
                storeService.addStore(store);
            }
        }
        resp.sendRedirect("/server_war_exploded/stores");
    }
}
