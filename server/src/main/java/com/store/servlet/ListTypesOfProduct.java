package com.store.servlet;

import com.store.model.TypeProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListTypesOfProduct extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Todo complete: need add connect to database and get all entity products
        ProductService productService = new ProductServiceImpl();
        List<TypeProductEntity> types = productService.getAllTypeOfProduct();

        req.setAttribute("types", types);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/list_types_of_product.jsp");
        requestDispatcher.forward(req, resp);
    }
}
