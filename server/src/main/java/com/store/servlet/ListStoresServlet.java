package com.store.servlet;

import com.store.model.StoreEntity;
import com.store.service.store.StoreService;
import com.store.service.store.StoreServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ListStoresServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StoreService storeService = new StoreServiceImpl();
        List<StoreEntity> stores = storeService.getAllStores();
        req.setAttribute("stores", stores);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/list_stores.jsp");
        requestDispatcher.forward(req, resp);
    }
}
