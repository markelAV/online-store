package com.store.servlet;

import com.store.model.TypeUserEntity;
import com.store.model.UsersEntity;
import com.store.service.user.UserService;
import com.store.service.user.UserServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class AddUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = new UserServiceImpl();
        String id = req.getParameter("id");
        if (id != null) {
            UsersEntity user = userService.getUserById(Integer.parseInt(id));
            req.setAttribute("user", user);
        }
        List<TypeUserEntity> types = userService.getAllTypesUser();
        req.setAttribute("types", types);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/create_user.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("login").length() > 0) {
            String login = req.getParameter("login");
            String name = req.getParameter("name");
            String id = req.getParameter("id");
            String password = req.getParameter("password");
            int idRole = Integer.parseInt(req.getParameter("idRole"));
            UserService userService = new UserServiceImpl();
            TypeUserEntity type = new TypeUserEntity();
            type.setId(idRole);
            UsersEntity user = new UsersEntity(0, login, password, name, type);
            if (id != null) {
                user.setId(Integer.parseInt(id));
                userService.updateUserInformation(user);
            } else {
                userService.addUser(user);
            }
        }
        resp.sendRedirect("/server_war_exploded/users");
    }
}
