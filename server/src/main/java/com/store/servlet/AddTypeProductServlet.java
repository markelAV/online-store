package com.store.servlet;

import com.store.model.TypeProductEntity;
import com.store.service.product.ProductService;
import com.store.service.product.ProductServiceImpl;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddTypeProductServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id != null) {
            ProductService service = new ProductServiceImpl();
            TypeProductEntity type = service.findTypeById(Integer.parseInt(id));
            req.setAttribute("type", type);
        }
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/create_type_of_product.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("name").length() > 0) {
            String name = req.getParameter("name");
            String description = req.getParameter("description");
            String id = req.getParameter("id");

            TypeProductEntity type = new TypeProductEntity();
            type.setName(name);
            if (!description.equals("")) {
                type.setDescription(description);
            }

            ProductService productService = new ProductServiceImpl();
            if (id != null) {
                type.setId(Integer.parseInt(id));
                productService.updateTypeProduct(type);

            } else {
                productService.addTypeOfProduct(type);
            }
        }
        resp.sendRedirect("/server_war_exploded/types");
    }
}
