package com.store.servlet;

import com.store.model.TypeProductEntity;
import com.store.service.product.*;
import com.store.utils.ShoppingUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class CatalogTypesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProductService productService = new ProductServiceImpl();
        List<TypeProductEntity> types = productService.getAllTypeOfProduct();
        req.setAttribute("types_product", types);
        req.setAttribute("countOrder", ShoppingUtil.countProducts(req));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("pages/types_product.jsp");
        requestDispatcher.forward(req, resp);
    }
}
