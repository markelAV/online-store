package com.store.dao.user;

import com.store.model.TypeUserEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class TypeUserDaoImpl implements TypeUserDao {

    @Override
    public void addTypeUser(TypeUserEntity typeUserEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(typeUserEntity);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteTypeUser(int typeId) {
        TypeUserEntity typeUserEntity = getTypeByIdType(typeId);
        if (typeUserEntity != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(typeUserEntity);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public void deleteTypeUser(TypeUserEntity typeUserEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(typeUserEntity);
        session.close();
    }

    @Override
    public void updateTypeUser(TypeUserEntity typeUserEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(typeUserEntity);
        tx1.commit();
        session.close();
    }

    @Override
    public TypeUserEntity getTypeByIdType(int id) {
        TypeUserEntity typeUserEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(TypeUserEntity.class, id);
        return typeUserEntity;
    }

    @Override
    public List<TypeUserEntity> getAllTypesUser() {
        List typeUserEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From TypeUserEntity ")
                .list();
        return typeUserEntity;
    }
}
