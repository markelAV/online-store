package com.store.dao.user;

import com.store.model.UsersEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class UserDaoImpl implements UserDao {
    public void addUser(UsersEntity usersEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(usersEntity);
        transaction.commit();
        session.close();
    }

    public void deleteUser(int userId) {
        UsersEntity productEntity = getUserById(userId);
        if (productEntity != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(productEntity);
            transaction.commit();
            session.close();
        }
    }

    public void deleteUser(UsersEntity usersEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(usersEntity);
        transaction.commit();
        session.close();
    }

    public UsersEntity getUserById(int userId) {
        UsersEntity usersEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(UsersEntity.class, userId);
        return usersEntity;
    }

    public void updateUser(UsersEntity usersEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(usersEntity);
        tx1.commit();
        session.close();
    }

    public List<UsersEntity> getAllUser() {
        List usersEntitys = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From UsersEntity ")
                .list();
        return usersEntitys;
    }

    public List<UsersEntity> getUsersByRole(int idRole) {
        List<UsersEntity> usersEntitys = new LinkedList<UsersEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from ProductEntity where id_type_user = :idRole");
        query.setParameter("idRole", idRole);
        usersEntitys = query.getResultList();
        return usersEntitys;
    }
}
