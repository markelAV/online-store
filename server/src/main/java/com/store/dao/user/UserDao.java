package com.store.dao.user;

import com.store.model.UsersEntity;

import java.util.List;

public interface UserDao {
    void addUser(UsersEntity usersEntity);

    void deleteUser(int userId);

    void deleteUser(UsersEntity usersEntity);

    UsersEntity getUserById(int userId);

    void updateUser(UsersEntity usersEntity);

    List<UsersEntity> getAllUser();

    List<UsersEntity> getUsersByRole(int idRole);
}
