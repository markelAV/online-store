package com.store.dao.user;

import com.store.model.TypeUserEntity;
import com.store.model.UsersEntity;

import java.util.List;

public interface TypeUserDao {

    void addTypeUser(TypeUserEntity typeUserEntity);

    void deleteTypeUser(int typeId);

    void deleteTypeUser(TypeUserEntity typeUserEntity);

    void updateTypeUser(TypeUserEntity typeUserEntity);

    TypeUserEntity getTypeByIdType(int id);

    List<TypeUserEntity> getAllTypesUser();
}
