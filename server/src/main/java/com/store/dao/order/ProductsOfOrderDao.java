package com.store.dao.order;

import com.store.model.ProductsOfOrderEntity;

import java.util.List;

public interface ProductsOfOrderDao {
    void addProductInOrder(ProductsOfOrderEntity productsOfOrderEntity);

    void deleteProductInOrder(int idOrder, int idProduct);

    void changeQuantityOfProducts(ProductsOfOrderEntity order);

    List<ProductsOfOrderEntity> getProductsOfOrderById(int idOrder);
}
