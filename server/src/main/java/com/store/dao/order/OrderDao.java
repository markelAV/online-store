package com.store.dao.order;

import com.store.model.OrdersEntity;

import java.util.List;

public interface OrderDao {
    void addOrder(OrdersEntity order);

    List<OrdersEntity> getAllOrders();

    void deleteOrderById(int id);

    OrdersEntity findOrderById(int id);

    void updateOrder(OrdersEntity order);

    List<OrdersEntity> getOrdersByStore(int idStore);

    List<OrdersEntity> getOrdersByStatus(String nameStatus);

    List<OrdersEntity> getOrdersByIdUser(int idUser);
}
