package com.store.dao.order;

import com.store.model.ProductsOfOrderEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class ProductsOfOrderDaoImpl implements ProductsOfOrderDao {
    @Override
    public void addProductInOrder(ProductsOfOrderEntity productsOfOrderEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(productsOfOrderEntity);
        tx1.commit();
        session.close();
    }

    @Override
    public void deleteProductInOrder(int idOrder, int idProduct) {
        ProductsOfOrderEntity product = getProductOfOrderByIds(idOrder, idProduct);
        if (product != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            session.delete(product);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public void changeQuantityOfProducts(ProductsOfOrderEntity productsOfOrderEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(productsOfOrderEntity);
        tx1.commit();
        session.close();
    }

    @Override
    public List<ProductsOfOrderEntity> getProductsOfOrderById(int idOrder) {
        List<ProductsOfOrderEntity> products = new LinkedList<>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from ProductsOfOrderEntity where id_order = :idOrder");
        query.setParameter("idOrder", idOrder);
        products = query.getResultList();
        session.close();
        return products;
    }

    /**
     * Find ProductsOfOrderEntity by ids Order and Products
     * If entity not found then return null
     *
     * @param idOrder
     * @param idProduct
     * @return ProductsOfOrderEntity or null
     */
    public ProductsOfOrderEntity getProductOfOrderByIds(int idOrder, int idProduct) {
        List<ProductsOfOrderEntity> products = new LinkedList<>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from ProductsOfOrderEntity where id_order = :idOrder and id_product = :idProduct");
        query.setParameter("idOrder", idOrder);
        query.setParameter("idProduct", idProduct);
        products = query.getResultList();
        session.close();
        return products.size() > 0 ? products.get(0) : null;
    }
}
