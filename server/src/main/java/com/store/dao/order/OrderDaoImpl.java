package com.store.dao.order;

import com.store.model.OrdersEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class OrderDaoImpl implements OrderDao {
    public void addOrder(OrdersEntity order) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(order);
        tx1.commit();
        session.close();
    }

    public List<OrdersEntity> getAllOrders() {
        List<OrdersEntity> orders = (List<OrdersEntity>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From OrdersEntity ")
                .list();
        return orders;
    }

    public void deleteOrderById(int id) {
        OrdersEntity user = findOrderById(id);
        if (user != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            session.delete(user);
            tx1.commit();
            session.close();
        }
    }

    public OrdersEntity findOrderById(int id) {
        OrdersEntity user = HibernateSessionFactoryUtil.getSessionFactory().openSession().get(OrdersEntity.class, id);
        return user;
    }

    public void updateOrder(OrdersEntity order) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(order);
        tx1.commit();
        session.close();
    }

    public List<OrdersEntity> getOrdersByStore(int idStore) {
        List<OrdersEntity> orders = new LinkedList<OrdersEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from OrdersEntity where id_store = :idStore");
        query.setParameter("idStore", idStore);
        orders = query.getResultList();
        session.close();
        return orders;
    }

    public List<OrdersEntity> getOrdersByStatus(String nameStatus) {
        List<OrdersEntity> orders = new LinkedList<OrdersEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from OrdersEntity where status = :nameStatus");
        query.setParameter("nameStatus", nameStatus);
        orders = query.getResultList();
        session.close();
        return orders;
    }

    public List<OrdersEntity> getOrdersByIdUser(int idUser) {
        List<OrdersEntity> orders = new LinkedList<OrdersEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from OrdersEntity where id_user = :idUser");
        query.setParameter("idUser", idUser);
        orders = query.getResultList();
        session.close();
        return orders;
    }
}
