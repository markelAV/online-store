package com.store.dao.store;

import com.store.model.StoreEntity;

import java.util.List;

public interface StoreDao {
    void addStore(StoreEntity storeEntity);

    void deleteStore(int storeId);

    void deleteStore(StoreEntity storeEntity);

    StoreEntity getStoreById(int storeId);

    void updateStore(StoreEntity storeEntity);

    List<StoreEntity> getAllStores();
}
