package com.store.dao.store;

import com.store.model.StockEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;

public class StockDaoImpl implements StockDao {
    @Override
    public void addProduct(int idProduct, int idStore, int count) {
        StockEntity stockEntity = new StockEntity(idStore, idProduct, count);
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(stockEntity);
        transaction.commit();
        session.close();
    }

    @Override
    public void addProduct(int idProduct, int storeId) {
        addProduct(idProduct, storeId, 0);
    }

    @Override
    public void changeQuantity(int idProduct, int idStore, int count) {
        StockEntity stockEntity = new StockEntity(idStore, idProduct, count);
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(stockEntity);
        tx1.commit();
        session.close();
    }

    @Override
    public void deleteProduct(int idProduct, int idStore) {
        StockEntity stockEntity = getStockEntityByIds(idProduct, idStore);
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(stockEntity);
        transaction.commit();
        session.close();
    }

    private StockEntity getStockEntityByIds(int idProduct, int idStore) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery(" from StockEntity where id_store = :idStore and id_product = : idProduct");
        query.setParameter("idStore", idStore);
        query.setParameter("idProduct", idProduct);
        return (StockEntity) query.getSingleResult();
    }

    @Override
    public int getQuantityProduct(int idProduct, int idStore) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("select quantity from StockEntity where id_store = :idStore and id_product = : idProduct");
        query.setParameter("idStore", idStore);
        query.setParameter("idProduct", idProduct);
        int result = (Integer) query.getSingleResult();
        return result;
    }
}
