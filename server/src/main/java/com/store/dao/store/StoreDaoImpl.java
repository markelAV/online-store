package com.store.dao.store;

import com.store.model.StoreEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class StoreDaoImpl implements StoreDao {
    @Override
    public void addStore(StoreEntity storeEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(storeEntity);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteStore(int storeId) {
        StoreEntity storeEntity = getStoreById(storeId);
        if (storeEntity != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(storeEntity);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public void deleteStore(StoreEntity storeEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(storeEntity);
        transaction.commit();
        session.close();
    }

    @Override
    public StoreEntity getStoreById(int storeId) {
        StoreEntity storeEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(StoreEntity.class, storeId);
        return storeEntity;
    }

    @Override
    public void updateStore(StoreEntity storeEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(storeEntity);
        tx1.commit();
        session.close();
    }

    @Override
    public List<StoreEntity> getAllStores() {
        List storeEntitys = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From StoreEntity ")
                .list();
        return storeEntitys;
    }
}
