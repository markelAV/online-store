package com.store.dao.store;

public interface StockDao {

    void addProduct(int idProduct, int storeId);

    void addProduct(int idProduct, int storeId, int count);

    void changeQuantity(int idProduct, int storeId, int count);

    void deleteProduct(int idProduct, int storeId);

    int getQuantityProduct(int idProduct, int idStore);

}
