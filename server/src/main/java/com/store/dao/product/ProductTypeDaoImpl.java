package com.store.dao.product;

import com.store.model.TypeProductEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ProductTypeDaoImpl implements ProductTypeDao {
    public void addTypeOfProduct(TypeProductEntity typeProductEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(typeProductEntity);
        transaction.commit();
        session.close();
    }

    public List<TypeProductEntity> getAllTypeOfProduct() {
        List typeProductEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From TypeProductEntity ")
                .list();
        return typeProductEntity;
    }

    public void deleteTypeById(int id) {
        TypeProductEntity productEntity = findTypeById(id);
        if (productEntity != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(productEntity);
            transaction.commit();
            session.close();
        }
    }

    public TypeProductEntity findTypeById(int id) {
        TypeProductEntity typeProductEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(TypeProductEntity.class, id);
        return typeProductEntity;
    }

    public void updateTypeProduct(TypeProductEntity typeProductEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(typeProductEntity);
        tx1.commit();
        session.close();
    }
}
