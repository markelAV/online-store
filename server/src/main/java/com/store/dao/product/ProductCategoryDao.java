package com.store.dao.product;

import com.store.model.CategoryProductEntity;

import java.util.List;

public interface ProductCategoryDao {
    void addCategoryOfProduct(CategoryProductEntity categoryProductEntity);

    List<CategoryProductEntity> getAllCategoryOfProduct();

    void deleteCategoryById(int id);

    CategoryProductEntity findCategoryById(int id);

    void updateCategoryOfProduct(CategoryProductEntity categoryProductEntity);

    List<CategoryProductEntity> searchCategoriesByType(int idType);

}
