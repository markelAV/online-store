package com.store.dao.product;

import com.store.model.TypeProductEntity;

import java.util.List;

public interface ProductTypeDao {
    void addTypeOfProduct(TypeProductEntity typeProductEntity);

    List<TypeProductEntity> getAllTypeOfProduct();

    void deleteTypeById(int id);

    TypeProductEntity findTypeById(int id);

    void updateTypeProduct(TypeProductEntity typeProductEntity);
}
