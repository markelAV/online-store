package com.store.dao.product;

import com.store.model.ProductEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import java.util.LinkedList;
import java.util.List;

public class ProductDaoImpl implements ProductDao {

    public ProductDaoImpl() {
    }

    public void addProduct(ProductEntity productEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(productEntity);
        transaction.commit();
        session.close();
    }

    public List<ProductEntity> getAllProducts() {
        List productEntities = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From ProductEntity ")
                .list();
        return productEntities;
    }

    public void deleteProductById(int id) {
        ProductEntity productEntity = findProductById(id);
        if (productEntity != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(productEntity);
            transaction.commit();
            session.close();
        }
    }

    public ProductEntity findProductById(int id) {
        ProductEntity productEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(ProductEntity.class, id);
        return productEntity;
    }

    public void updateProduct(ProductEntity productEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(productEntity);
        tx1.commit();
        session.close();
    }

    public List<ProductEntity> searchProductsByCategory(int idCategory) {
        List<ProductEntity> products = new LinkedList<ProductEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from ProductEntity where id_category_product = :idCategory");
        query.setParameter("idCategory", idCategory);
        products = query.getResultList();
        return products;
    }

    public List<ProductEntity> findProductByIds(List<Integer> ids) {
        List<ProductEntity> products = new LinkedList<ProductEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from ProductEntity product where product.id IN (:ids)");
        query.setParameterList("ids", ids);
        products = query.getResultList();
        return products;
    }

    public List<ProductEntity> searchProductsByTypeId(int idType) {
        return null;
    }
}
