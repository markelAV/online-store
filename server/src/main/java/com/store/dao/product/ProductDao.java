package com.store.dao.product;

import com.store.model.ProductEntity;

import java.util.List;

public interface ProductDao {
    void addProduct(ProductEntity productEntity);

    List<ProductEntity> getAllProducts();

    void deleteProductById(int id);

    ProductEntity findProductById(int id);

    void updateProduct(ProductEntity productEntity);

    List<ProductEntity> searchProductsByCategory(int idCategory);

    List<ProductEntity> findProductByIds(List<Integer> ids);

    List<ProductEntity> searchProductsByTypeId(int idType);
    //Todo add query for search products by type or category
}
