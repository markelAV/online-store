package com.store.dao.product;

import com.store.model.CategoryProductEntity;
import com.store.model.ProductEntity;
import com.store.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

public class ProductCategoryDaoImpl implements ProductCategoryDao {
    public void addCategoryOfProduct(CategoryProductEntity categoryProductEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(categoryProductEntity);
        transaction.commit();
        session.close();
    }

    public List<CategoryProductEntity> getAllCategoryOfProduct() {
        List categoryProductEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("From CategoryProductEntity ")
                .list();
        return categoryProductEntity;
    }

    public void deleteCategoryById(int id) {
        CategoryProductEntity categoryProductEntity = findCategoryById(id);
        if (categoryProductEntity != null) {
            Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(categoryProductEntity);
            transaction.commit();
            session.close();
        }
    }

    public CategoryProductEntity findCategoryById(int id) {
        CategoryProductEntity categoryProductEntity = HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(CategoryProductEntity.class, id);
        return categoryProductEntity;
    }

    public void updateCategoryOfProduct(CategoryProductEntity categoryProductEntity) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(categoryProductEntity);
        tx1.commit();
        session.close();
    }

    @Override
    public List<CategoryProductEntity> searchCategoriesByType(int idType) {
        List<CategoryProductEntity> categories = new LinkedList<CategoryProductEntity>();
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from CategoryProductEntity where id_type_product = :idType");
        query.setParameter("idType", idType);
        categories = query.getResultList();
        return categories;
    }
}
