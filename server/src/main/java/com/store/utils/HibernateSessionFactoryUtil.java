package com.store.utils;

import com.store.model.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Class for create factory of session
 */
public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(CategoryProductEntity.class);
                configuration.addAnnotatedClass(OrdersEntity.class);
                configuration.addAnnotatedClass(ProductEntity.class);
                configuration.addAnnotatedClass(ProductsOfOrderEntity.class);
                configuration.addAnnotatedClass(ProductsOfOrderEntityPK.class);
                configuration.addAnnotatedClass(StockEntity.class);
                configuration.addAnnotatedClass(StockEntityPK.class);
                configuration.addAnnotatedClass(StoreEntity.class);
                configuration.addAnnotatedClass(TypeProductEntity.class);
                configuration.addAnnotatedClass(TypeUserEntity.class);
                configuration.addAnnotatedClass(UsersEntity.class);
                configuration.addAnnotatedClass(WorkersEntity.class);
                configuration.addAnnotatedClass(WorkersEntityPK.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());

            } catch (Exception e) {
                System.out.println("Exception!" + e);
            }
        }
        return sessionFactory;
    }
}
