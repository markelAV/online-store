package com.store.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

public class ShoppingUtil {
    public static String countProducts(HttpServletRequest req) {
        String count = "0";
        HttpSession session = req.getSession();
        HashMap<String, Integer> productEntities = (HashMap<String, Integer>) session.getAttribute("ids");
        if (productEntities != null) {
            count = String.valueOf(productEntities.size());
        }
        return count;
    }
}
