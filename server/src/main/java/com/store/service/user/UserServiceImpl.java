package com.store.service.user;

import com.store.dao.user.TypeUserDao;
import com.store.dao.user.TypeUserDaoImpl;
import com.store.dao.user.UserDao;
import com.store.dao.user.UserDaoImpl;
import com.store.model.TypeUserEntity;
import com.store.model.UsersEntity;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao = new UserDaoImpl();
    private TypeUserDao typeUserDao = new TypeUserDaoImpl();

    /**
     * Create user
     *
     * @param usersEntity
     */
    public void addUser(UsersEntity usersEntity) {
        userDao.addUser(usersEntity);
    }

    /**
     * Remove user
     *
     * @param usersEntity
     */
    public void deleteUser(UsersEntity usersEntity) {
        userDao.deleteUser(usersEntity);
    }

    /**
     * Remove user by his id
     *
     * @param userId
     */
    public void deleteUser(int userId) {
        userDao.deleteUser(userId);
    }

    /**
     * Get user by id of User
     *
     * @param userId
     * @return
     */
    public UsersEntity getUserById(int userId) {
        return userDao.getUserById(userId);
    }

    /**
     * Update user (information about user)
     *
     * @param usersEntity
     */
    public void updateUserInformation(UsersEntity usersEntity) {
        userDao.updateUser(usersEntity);
    }

    /**
     * Get all users
     *
     * @return
     */
    public List<UsersEntity> getAllUser() {
        return userDao.getAllUser();
    }

    /**
     * Change role
     *
     * @param nameRole - is name role
     * @param idUser   - id user which that change role
     */
    public void changeRoleOfUser(String nameRole, int idUser) {
        //Fixme please complete logic this method
    }

    /**
     * Search users by id role
     *
     * @param idRole
     * @return
     */
    public List<UsersEntity> getUsersByRole(int idRole) {
        return userDao.getUsersByRole(idRole);
    }

    /**
     * Search users by name role
     *
     * @param nameRole
     * @return
     */
    public List<UsersEntity> getUsersByRole(String nameRole) {

        //Fixme please complete logic this method
        return null;
    }

    /**
     * Change password
     *
     * @param idUser      - id user which than change password
     * @param newPassword -new password for user
     */
    public void changePasswordUser(int idUser, String newPassword) {

    }

    @Override
    public TypeUserEntity getRoleByIdType(int id) {
        return typeUserDao.getTypeByIdType(id);
    }

    @Override
    public List<TypeUserEntity> getAllTypesUser() {
        return typeUserDao.getAllTypesUser();
    }
}
