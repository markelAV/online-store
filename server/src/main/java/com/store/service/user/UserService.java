package com.store.service.user;

import com.store.model.TypeUserEntity;
import com.store.model.UsersEntity;

import java.util.List;

public interface UserService {

    /**
     * Create user
     *
     * @param usersEntity
     */
    void addUser(UsersEntity usersEntity);

    /**
     * Remove user
     *
     * @param usersEntity
     */
    void deleteUser(UsersEntity usersEntity);

    /**
     * Remove user by his id
     *
     * @param userId
     */
    void deleteUser(int userId);

    /**
     * Get user by id of User
     *
     * @param userId
     * @return
     */
    UsersEntity getUserById(int userId);

    /**
     * Update user (information about user)
     *
     * @param usersEntity
     */
    void updateUserInformation(UsersEntity usersEntity);

    /**
     * Get all users
     *
     * @return
     */
    List<UsersEntity> getAllUser();

    /**
     * Change role
     *
     * @param nameRole - is name role
     * @param idUser   - id user which that change role
     */
    void changeRoleOfUser(String nameRole, int idUser);

    /**
     * Search users by id role
     *
     * @param idRole
     * @return
     */
    List<UsersEntity> getUsersByRole(int idRole);

    /**
     * Search users by name role
     *
     * @param nameRole
     * @return
     */
    List<UsersEntity> getUsersByRole(String nameRole);

    /**
     * Change password
     *
     * @param idUser      - id user which than change password
     * @param newPassword -new password for user
     */
    void changePasswordUser(int idUser, String newPassword); //Todo maybe remove??

    TypeUserEntity getRoleByIdType(int id);

    List<TypeUserEntity> getAllTypesUser();
}
