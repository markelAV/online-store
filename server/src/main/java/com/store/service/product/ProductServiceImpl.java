package com.store.service.product;

import com.store.dao.product.*;
import com.store.model.CategoryProductEntity;
import com.store.model.ProductEntity;
import com.store.model.TypeProductEntity;

import java.util.LinkedList;
import java.util.List;

public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;

    private ProductCategoryDao productCategoryDao;

    private ProductTypeDao productTypeDao;

    public ProductServiceImpl() {
        this.productDao = new ProductDaoImpl();
        this.productCategoryDao = new ProductCategoryDaoImpl();
        this.productTypeDao = new ProductTypeDaoImpl();
    }

    public void addProduct(ProductEntity productEntity) {
        productDao.addProduct(productEntity);
    }

    /**
     * inherit
     */
    public void addCategoryOfProduct(CategoryProductEntity categoryProductEntity) {
        productCategoryDao.addCategoryOfProduct(categoryProductEntity);
    }

    /**
     * Create Type of product and save his in DataBase
     *
     * @param typeProductEntity is new Type of Product
     */
    public void addTypeOfProduct(TypeProductEntity typeProductEntity) {
        productTypeDao.addTypeOfProduct(typeProductEntity);
    }

    public List<ProductEntity> getAllProducts() {
        return productDao.getAllProducts();
    }

    public List<ProductEntity> findProductByIds(List<Integer> ids) {
        return productDao.findProductByIds(ids);
    }

    /**
     * Get All categories of product
     */
    public List<CategoryProductEntity> getAllCategoryOfProduct() {
        return productCategoryDao.getAllCategoryOfProduct();
    }

    /**
     * Get All types of product
     */
    public List<TypeProductEntity> getAllTypeOfProduct() {
        return productTypeDao.getAllTypeOfProduct();
    }

    /**
     * Search Categories by id of type
     *
     * @param idType - type id
     * @return list categories
     */
    @Override
    public List<CategoryProductEntity> searchCategoriesByType(int idType) {
        return productCategoryDao.searchCategoriesByType(idType);
    }

    public void deleteProductById(int id) {
        productDao.deleteProductById(id);
    }

    /**
     * Delete Category from DataBase
     *
     * @param id is id of Category witch that need remove
     */
    public void deleteCategoryById(int id) {
        productCategoryDao.deleteCategoryById(id);
    }

    /**
     * Delete Type from DataBase
     *
     * @param id is id of Type witch that need remove
     */
    public void deleteTypeById(int id) {
        productTypeDao.deleteTypeById(id);
    }

    public ProductEntity findProductById(int id) {
        return productDao.findProductById(id);
    }

    /**
     * Find Category of product by his id
     *
     * @param id is id of Category product
     */
    public CategoryProductEntity findCategoryById(int id) {
        return productCategoryDao.findCategoryById(id);
    }

    /**
     * Find Type of product by his id
     *
     * @param id is id of Type product
     */
    public TypeProductEntity findTypeById(int id) {
        return productTypeDao.findTypeById(id);
    }

    public void updateProduct(ProductEntity productEntity) {
        productDao.updateProduct(productEntity);
    }

    /**
     * Update information about category of products (is- is read only)
     *
     * @param categoryProductEntity is new information of Category product
     */
    public void updateCategoryOfProduct(CategoryProductEntity categoryProductEntity) {
        productCategoryDao.updateCategoryOfProduct(categoryProductEntity);
    }

    /**
     * Update information about type of products (is- is read only)
     *
     * @param typeProductEntity is new information of Type product
     */
    public void updateTypeProduct(TypeProductEntity typeProductEntity) {
        productTypeDao.updateTypeProduct(typeProductEntity);
    }

    /**
     * Search products by idType
     *
     * @param productId is id of Product
     * @param incValue  is inc value
     */
    public void changeQuantityOfProduct(int productId, int incValue) {
        //Fixme please complete
    }

    /**
     * Search products by idType
     *
     * @param idCategory is id of Categories
     */
    public List<ProductEntity> searchProductsByCategory(int idCategory) {
        //Fixme please check exist product category
        //if exist then productDao.seatchProductByCategory(idCategory) else return null??

        return productDao.searchProductsByCategory(idCategory);
    }

    /**
     * Search products by idType
     *
     * @param idType is id of Types
     */
    public List<ProductEntity> searchProductsByTypeId(int idType) {
        List<CategoryProductEntity> categories = productCategoryDao.searchCategoriesByType(idType);
        List<ProductEntity> products = null;

        if (categories != null && categories.size() > 0) {
            // todo bad practice maybe using hashmap with key id product
            products = new LinkedList<>();
            for (CategoryProductEntity category : categories) {
                List<ProductEntity> tempProducts = productDao.searchProductsByCategory(category.getId());
                products.addAll(tempProducts);
            }
        }
        return products;
    }
}
