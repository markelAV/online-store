package com.store.service.product;

import com.store.model.CategoryProductEntity;
import com.store.model.ProductEntity;
import com.store.model.TypeProductEntity;

import java.util.List;

public interface ProductService {

    /**
     * Create product and save his in DataBase
     *
     * @param productEntity is new Product
     */
    void addProduct(ProductEntity productEntity);

    /**
     * Create Category of product and save his in DataBase
     *
     * @param categoryProductEntity is new Category of Product
     */
    void addCategoryOfProduct(CategoryProductEntity categoryProductEntity);

    /**
     * Create Type of product and save his in DataBase
     *
     * @param typeProductEntity is new Type of Product
     */
    void addTypeOfProduct(TypeProductEntity typeProductEntity);

    /**
     * Delete Product from DataBase
     *
     * @param id is id of Product witch that need remove
     */
    void deleteProductById(int id);

    /**
     * Delete Category from DataBase
     *
     * @param id is id of Category witch that need remove
     */
    void deleteCategoryById(int id);

    /**
     * Delete Type from DataBase
     *
     * @param id is id of Type witch that need remove
     */
    void deleteTypeById(int id);

    /**
     * Find product by his id
     *
     * @param id is id of Product
     */
    ProductEntity findProductById(int id);

    /**
     * Find Category of product by his id
     *
     * @param id is id of Category product
     */
    CategoryProductEntity findCategoryById(int id);

    /**
     * Find Type of product by his id
     *
     * @param id is id of Type product
     */
    TypeProductEntity findTypeById(int id);

    List<ProductEntity> findProductByIds(List<Integer> ids);

    /**
     * Get All products
     */
    List<ProductEntity> getAllProducts();

    /**
     * Get All categories of product
     */
    List<CategoryProductEntity> getAllCategoryOfProduct();

    /**
     * Get All types of product
     */
    List<TypeProductEntity> getAllTypeOfProduct();

    /**
     * Search Categories by id of type
     *
     * @param idType - type id
     * @return list categories
     */
    List<CategoryProductEntity> searchCategoriesByType(int idType);

    /**
     * Update information about products (is- is read only)
     *
     * @param productEntity is new information of Product
     */
    void updateProduct(ProductEntity productEntity);

    /**
     * Update information about category of products (is- is read only)
     *
     * @param categoryProductEntity is new information of Category product
     */
    void updateCategoryOfProduct(CategoryProductEntity categoryProductEntity);

    /**
     * Update information about type of products (is- is read only)
     *
     * @param typeProductEntity is new information of Type product
     */
    void updateTypeProduct(TypeProductEntity typeProductEntity);

    /**
     * Change quantity of products (update)
     *
     * @param productId is id of Product
     * @param incValue  is inc value
     */
    void changeQuantityOfProduct(int productId, int incValue);

    /**
     * Search products by idCategory
     *
     * @param idCategory is id of Categories
     */
    List<ProductEntity> searchProductsByCategory(int idCategory);

    /**
     * Search products by idType
     *
     * @param idType is id of Types
     */
    List<ProductEntity> searchProductsByTypeId(int idType);

    //Todo add query for search products by type or category
}
