package com.store.service.store;

import com.store.dao.store.StockDao;
import com.store.dao.store.StockDaoImpl;
import com.store.dao.store.StoreDao;
import com.store.dao.store.StoreDaoImpl;
import com.store.model.StoreEntity;

import java.util.List;

public class StoreServiceImpl implements StoreService {

    private StoreDao storeDao = new StoreDaoImpl();

    private StockDao stockDao = new StockDaoImpl();

    @Override
    public void addStore(StoreEntity storeEntity) {
        storeDao.addStore(storeEntity);
    }

    @Override
    public void deleteStore(int storeId) {
        storeDao.deleteStore(storeId);
    }

    @Override
    public void deleteStore(StoreEntity storeEntity) {
        storeDao.deleteStore(storeEntity);
    }

    @Override
    public StoreEntity getStoreById(int storeId) {
        return storeDao.getStoreById(storeId);
    }

    @Override
    public void updateStore(StoreEntity storeEntity) {
        storeDao.updateStore(storeEntity);
    }

    @Override
    public List<StoreEntity> getAllStores() {
        return storeDao.getAllStores();
    }

    @Override
    public void addProduct(int idProduct, int storeId) {
        stockDao.addProduct(idProduct, storeId);
    }

    @Override
    public void addProduct(int idProduct, int storeId, int count) {
        stockDao.addProduct(idProduct, storeId, count);
    }

    @Override
    public void changeQuantity(int idProduct, int storeId, int count) {
        stockDao.changeQuantity(idProduct, storeId, count);
    }

    @Override
    public void deleteProduct(int idProduct, int storeId) {
        stockDao.deleteProduct(idProduct, storeId);
    }

    @Override
    public int getQuantityProduct(int idProduct, int idStore) {
        return stockDao.getQuantityProduct(idProduct, idStore);
    }
}
