package com.store.service.store;

import com.store.model.StoreEntity;

import java.util.List;

public interface StoreService {
    void addStore(StoreEntity storeEntity);

    void deleteStore(int storeId);

    void deleteStore(StoreEntity storeEntity);

    StoreEntity getStoreById(int storeId);

    void updateStore(StoreEntity storeEntity);

    List<StoreEntity> getAllStores();

    void addProduct(int idProduct, int storeId);

    void addProduct(int idProduct, int storeId, int count);

    void changeQuantity(int idProduct, int storeId, int count);

    void deleteProduct(int idProduct, int storeId);

    int getQuantityProduct(int idProduct, int idStore);

}
