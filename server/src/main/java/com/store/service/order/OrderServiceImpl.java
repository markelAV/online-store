package com.store.service.order;

import com.store.dao.order.OrderDao;
import com.store.dao.order.OrderDaoImpl;
import com.store.dao.order.ProductsOfOrderDao;
import com.store.dao.order.ProductsOfOrderDaoImpl;
import com.store.model.OrdersEntity;
import com.store.model.ProductEntity;
import com.store.model.ProductsOfOrderEntity;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao = new OrderDaoImpl();

    private ProductsOfOrderDao productsOfOrderDao = new ProductsOfOrderDaoImpl();

    /**
     * Create order
     *
     * @param orderEntity
     */
    public void addOrder(OrdersEntity orderEntity) {
        orderDao.addOrder(orderEntity);
    }

    /**
     * Add products id Order
     *
     * @param productsOfOrderEntity
     */
    @Override
    public void addProductInOrder(ProductsOfOrderEntity productsOfOrderEntity) {
        productsOfOrderDao.addProductInOrder(productsOfOrderEntity);
    }

    /**
     * Remove order
     *
     * @param orderEntity
     */
    public void deleteOrder(OrdersEntity orderEntity) {
        orderDao.deleteOrderById(orderEntity.getId());
    }

    /**
     * Remove order by idOrder
     *
     * @param idOrder
     */
    public void deleteOrder(int idOrder) {
        orderDao.deleteOrderById(idOrder);
    }

    /**
     * Delete products from order
     *
     * @param idOrder   is id order from which need remove product
     * @param idProduct is id product which need remove from order
     */
    @Override
    public void deleteProductInOrder(int idOrder, int idProduct) {
        productsOfOrderDao.deleteProductInOrder(idOrder, idProduct);
    }

    /**
     * Update information about order
     *
     * @param orderEntity
     */
    public void updateOrder(OrdersEntity orderEntity) {
        orderDao.updateOrder(orderEntity);
    }

    /**
     * Change quantity products in order
     *
     * @param order
     */
    @Override
    public void changeQuantityOfProducts(ProductsOfOrderEntity order) {
        productsOfOrderDao.changeQuantityOfProducts(order);
    }

    /**
     * Get order by hid id
     *
     * @param idOrder
     * @return
     */
    public OrdersEntity getOrderById(int idOrder) {
        return orderDao.findOrderById(idOrder);
    }

    /**
     * Get all orders
     *
     * @return
     */
    public List<OrdersEntity> getAllOrders() {
        return orderDao.getAllOrders();
    }

    /**
     * Search orders by id of Store
     *
     * @param idStore
     * @return
     */
    public List<OrdersEntity> getOrdersByStore(int idStore) {
        return orderDao.getOrdersByStore(idStore);
    }

    /**
     * Search order by name status
     *
     * @param nameStatus - name status
     * @return
     */
    public List<OrdersEntity> getOrdersByStatus(String nameStatus) {
        return orderDao.getOrdersByStatus(nameStatus);
    }

    /**
     * Search orders by id User
     *
     * @param idUser
     * @return
     */
    public List<OrdersEntity> getOrdersByIdUser(int idUser) {
        return orderDao.getOrdersByIdUser(idUser);
    }

    /**
     * Get Products by id Order
     *
     * @param idOrder
     * @return
     */
    @Override
    public List<ProductsOfOrderEntity> getProductsOfOrderById(int idOrder) {
        return productsOfOrderDao.getProductsOfOrderById(idOrder);
    }

}
