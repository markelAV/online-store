package com.store.service.order;

import com.store.model.OrdersEntity;
import com.store.model.ProductsOfOrderEntity;

import java.util.List;

public interface OrderService {

    /**
     * Create order
     *
     * @param orderEntity
     */
    void addOrder(OrdersEntity orderEntity);

    /**
     * Add products id Order
     *
     * @param productsOfOrderEntity
     */
    void addProductInOrder(ProductsOfOrderEntity productsOfOrderEntity);

    /**
     * Remove order
     *
     * @param orderEntity
     */
    void deleteOrder(OrdersEntity orderEntity);

    /**
     * Remove order by idOrder
     *
     * @param idOrder
     */
    void deleteOrder(int idOrder);

    /**
     * Delete products from order
     *
     * @param idOrder   is id order from which need remove product
     * @param idProduct is id product which need remove from order
     */
    void deleteProductInOrder(int idOrder, int idProduct);

    /**
     * Update information about order
     *
     * @param orderEntity
     */
    void updateOrder(OrdersEntity orderEntity);

    /**
     * Change quantity products in order
     *
     * @param order
     */
    void changeQuantityOfProducts(ProductsOfOrderEntity order);

    /**
     * Get order by hid id
     *
     * @param idOrder
     * @return
     */
    OrdersEntity getOrderById(int idOrder);

    /**
     * Get all orders
     *
     * @return
     */
    List<OrdersEntity> getAllOrders();

    /**
     * Search orders by id of Store
     *
     * @param idStore
     * @return
     */
    List<OrdersEntity> getOrdersByStore(int idStore);

    /**
     * Search order by name status
     *
     * @param nameStatus - name status
     * @return
     */
    List<OrdersEntity> getOrdersByStatus(String nameStatus);

    /**
     * Search orders by id User
     *
     * @param idUser
     * @return
     */
    List<OrdersEntity> getOrdersByIdUser(int idUser);

    /**
     * Get Products by id Order
     *
     * @param idOrder
     * @return
     */
    List<ProductsOfOrderEntity> getProductsOfOrderById(int idOrder);

}
