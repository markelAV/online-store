package com.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "workers", schema = "public", catalog = "postgres")
@IdClass(WorkersEntityPK.class)
public class WorkersEntity {

    @Id
    @Column(name = "id_user", nullable = false)
    private int idUser;

    @Id
    @Column(name = "id_store", nullable = false)
    private int idStore;
}
