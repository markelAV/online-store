package com.store.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode
@Table(name = "product", schema = "public", catalog = "postgres")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "name", nullable = false, length = -1)
    private String name;

    @Basic
    @Column(name = "photo", nullable = true, length = -1)
    private String photo;

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    private String description;

    @Basic
    @Column(name = "price", nullable = false, precision = 0)
    private float price;

    @Basic
    @Column(name = "rating", nullable = false, precision = 0)
    private float rating;

    @ManyToOne
    @JoinColumn(name = "id_category_product", referencedColumnName = "id", nullable = false)
    private CategoryProductEntity categoryProduct;
}
