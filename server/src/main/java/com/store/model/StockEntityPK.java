package com.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class StockEntityPK implements Serializable {

    @Column(name = "id_store", nullable = false)
    @Id
    private int idStore;

    @Column(name = "id_product", nullable = false)
    @Id
    private int idProduct;
}
