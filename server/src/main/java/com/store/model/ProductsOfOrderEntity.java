package com.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "products_of_order", schema = "public", catalog = "postgres")
public class ProductsOfOrderEntity {

    @EmbeddedId
    private ProductsOfOrderEntityPK productIdOrder;

    @Basic
    @Column(name = "count", nullable = false)
    private int count;


    public ProductsOfOrderEntity(int idOrder, int idProduct, int count) {
        this.productIdOrder = new ProductsOfOrderEntityPK(idProduct, idOrder);
        this.count = count;
    }
}
