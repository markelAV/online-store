package com.store.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "stock", schema = "public", catalog = "postgres")
@IdClass(StockEntityPK.class)
public class StockEntity {
    @Id
    @Column(name = "id_store", nullable = false)
    private int idStore;

    @Id
    @Column(name = "id_product", nullable = false)
    private int idProduct;

    @Basic
    @Column(name = "quantity", nullable = false)
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "id_store", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private StoreEntity storeByIdStore;

    public StockEntity(int idStore, int idProduct, int quantity) {
        this.idProduct = idProduct;
        this.idStore = idStore;
        this.quantity = quantity;
    }
}
