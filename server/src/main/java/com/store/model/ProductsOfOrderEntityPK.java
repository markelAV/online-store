package com.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProductsOfOrderEntityPK implements Serializable {

    private int id_product;
    private int id_order;

    @Column(name = "id_product", nullable = false)
    @Id
    public int getId_product() {
        return id_product;
    }


    @Column(name = "id_order", nullable = false)
    @Id
    public int getId_order() {
        return id_order;
    }

}
