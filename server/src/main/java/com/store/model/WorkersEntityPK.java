package com.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class WorkersEntityPK implements Serializable {

    @Column(name = "id_user", nullable = false)
    @Id
    private int idUser;

    @Column(name = "id_store", nullable = false)
    @Id
    private int idStore;
}
