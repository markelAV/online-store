package com.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "store", schema = "public", catalog = "postgres")
public class StoreEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "address", nullable = false, length = -1)
    private String address;

    @Basic
    @Column(name = "working_hours", nullable = false, length = -1)
    private String workingHours;

    public StoreEntity(int id) {
        this.id = id;
    }
}
