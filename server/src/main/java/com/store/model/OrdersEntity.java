package com.store.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders", schema = "public", catalog = "postgres")
public class OrdersEntity {

    @Id
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "status", nullable = false, length = -1)
    private String status;

    @Basic
    @Column(name = "sum", nullable = false)
    private float sum;

    @ManyToOne
    @JoinColumn(name = "id_store", referencedColumnName = "id", nullable = false)
    private StoreEntity store;

    @ManyToOne
    @JoinColumn(name = "id_user", referencedColumnName = "id", nullable = false)
    private UsersEntity user;

}
