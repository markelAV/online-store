package com.store.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "category_product", schema = "public", catalog = "postgres")
@Data
@EqualsAndHashCode
public class CategoryProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Basic
    @Column(name = "name", nullable = false, length = -1)
    private String name;

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    private String description;

    @ManyToOne
    @JoinColumn(name = "id_type_product", referencedColumnName = "id", nullable = false)
    private TypeProductEntity typeProduct;

}
