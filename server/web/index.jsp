<%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 08.02.2020
  Time: 19:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>МузБар - Стартовая</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="script.js"></script>
    <link rel="import" href="html/header.html">
</head>
<body>
<!-- The Modal -->
<div id="userMenu" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="popup-menu-user">
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Профиль</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/orders">Заказы</a>
            </div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/types">Типы
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/categories">Категории
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/products">Продукты</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/users">Пользователи</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/stores">Магазины</a></div>
            <div class="popup-menu-user__item"><a href="/server_war_exploded/catalog-products?id=1">Тест каталог</a>
            </div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Выход</a></div>
        </div>
    </div>

</div>
<div class="header-welcome">
    <div class="header-menu">
        <div class="menu-part-left">
            <div class="logo">
                <img class="logo__img" lt="logo" src="images/logo.png"/>
                <a class="logo__text" href="/server_war_exploded">
                    МузБар
                </a>
            </div>
        </div>
        <div class="menu-part-middle-welcome"></div>
        <div class="menu-part-right">
            <div class="menu-items">
                <div class="menu-item">
                    <a class="menu-item__link" href="/server_war_exploded/types-of-products">
                        Каталог
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-item__link" href="/server_war_exploded/stores">
                        Магазины
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-item__link">
                        О Нас
                    </a>
                </div>
            </div>
            <div class="menu-profile">
                <button class="profile__button" onclick="openMenu()"></button>
                <div class="profile__name">
                    <a href="#" title="Dismissible popover" data-toggle="popover" data-placement="bottom"
                       data-trigger="focus"
                       data-content="<a class='menu-item__link' href='/server_war_exploded/orders'>Заказы</a>">Маркелов
                        А.</a>
                </div>
            </div>
            <div class="shopping-card">
                <a href="/server_war_exploded/shopping-card">
                    <img class="shopping-card__image"
                         src="images/shopping_card_icon.png"
                         alt="shopping_card"/></a>
            </div>
        </div>
    </div>
</div>
</div>
<div class="content">
    <div class="offer_block">
        <div class="offer_information">
            <div class="offer_information__header">
                Наполни свою жизнь новым звучанием
            </div>
            <div class="offer_information__description">
                Музыкальный Бар - магазин для настоящих ценителей прекрасного. Хороший и качественный инструмент
                подходящий как профессионалам, так и новичкам.
            </div>
            <div class="offer_control">
                <button id="but_offer" class="offer_control__button"
                        onclick="location.href='/server_war_exploded/types-of-products'">Наполнить!
                </button>
            </div>
        </div>
        <div class="offer_picture">
            <img class="offer_picture__image" src="images/offer_image.png" alt="offer">
        </div>
    </div>
</div>
</body>
</html>
