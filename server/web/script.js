// Get the modal

// Get the button that opens the modal

// When the user clicks on the button, open the modal
function openMenu() {
    var modal = document.getElementById("userMenu");
    var menu = document.getElementsByClassName("menu-profile")[0];
    modal.style.display = "block";
    modal.style.left = menu.offsetLeft;
    modal.style.top = menu.offsetTop + 25;
}

// When the user clicks on <span> (x), close the modal


// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    var modal = document.getElementById("userMenu");
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function pasteHead() {
    var link = document.querySelector('link[rel=import]');
    var content = link.import.querySelector('#header');
    document.body.appendChild(content.cloneNode(true));
}
