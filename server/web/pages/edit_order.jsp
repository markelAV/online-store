<%@ page import="com.store.model.OrdersEntity" %>
<%@ page import="com.store.model.UsersEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="com.store.model.StoreEntity" %><%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 23.04.2020
  Time: 19:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% OrdersEntity order = (OrdersEntity) request.getAttribute("order");
    List<UsersEntity> users = (List<UsersEntity>) request.getAttribute("users");
    List<StoreEntity> stores = (List<StoreEntity>) request.getAttribute("stores");
    String numberOrder = "";
    String status = "";
    float sum = -1.0f;
    int idStore = -1;
    int idUser = -1;
    if (order != null) {
        numberOrder = String.valueOf(order.getId());
        status = order.getStatus();
        sum = order.getSum();
        idStore = order.getStore().getId();
        idUser = order.getUser().getId();
    }
%>
<html>
<head>
    <title>
        Редактирование Заказа
    </title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/create_card_style.css">
    <script src="script.js"></script>
</head>
<body>
<!-- The Modal -->
<div id="userMenu" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="popup-menu-user">
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Профиль</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/orders">Заказы</a>
            </div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/types">Типы
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/categories">Категории
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/products">Продукты</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/users">Пользователи</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/stores">Магазины</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Выход</a></div>
        </div>
    </div>

</div>
<div class="header">
    <div class="header">
        <div class="header-menu">
            <div class="menu-part-left">
                <div class="logo">
                    <img class="logo__img" lt="logo" src="images/logo.png"/>
                    <a class="logo__text" href="/server_war_exploded">
                        МузБар
                    </a>
                </div>
            </div>
            <div class="menu-part-middle"></div>
            <div class="menu-part-right">
                <div class="menu-items">
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/types-of-products">
                            Каталог
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/stores">
                            Магазины
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link">
                            О Нас
                        </a>
                    </div>
                </div>
                <div class="menu-profile">
                    <button class="profile__button" onclick="openMenu()"></button>
                    <div class="profile__name">
                        <a href="#" title="Dismissible popover" data-toggle="popover" data-placement="bottom"
                           data-trigger="focus"
                           data-content="<a class='menu-item__link' href='/server_war_exploded/orders'>Заказы</a>">Маркелов
                            А.</a>
                    </div>
                </div>
                <div class="shopping-card">
                    <a href="/server_war_exploded/shopping-card">
                        <img class="shopping-card__image"
                             src="images/shopping_card_icon.png"
                             alt="shopping_card"/></a>
                    <% String count = (String) request.getAttribute("countOrder");
                        if (count != null && !count.equals("") && Integer.parseInt(count) > 0) {%>
                    <div class="around-count">
                        <div class="around-count__text">
                            <%=count%>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    <div class="header-line">

    </div>
</div>
<div class="content">
    <div class="edit-form">
        <div class="enter_card">
            <div class="header_enter_card">
                <div class="header_enter_card__title">
                    Редактирование категории продукта
                </div>
            </div>
            <div class="body_enter_card">
                <form class="card_form" method="post">
                    <intput id="id" name="id" value="<%=numberOrder%>>" type="text"
                            style="display: none"></intput>
                    <div class="card_form__field">
                        <label for="status">Статус</label>
                        <input id="status" name="status" type="text" required="true" value="<%=status%>">
                    </div>
                    <div class="card_form__field">
                        <label for="sum">Сумма заказа</label>
                        <input id="sum" name="sum" type="text" value="<%=sum%>">
                    </div>
                    <div class="card_form__field">
                        <label for="idStore">Город</label>
                        <select name="idStore" id="idStore" required="true">
                            <% if (stores != null) {
                                for (StoreEntity store : stores) { %>
                            <option value="<%=store.getId()%>"  <%=store.getId() == idStore ? "selected" : ""%>><%=store.getAddress()%>
                            </option>
                            <% }
                            }%>
                        </select>
                    </div>
                    <div class="card_form__field">
                        <label for="idUser">Пользователь</label>
                        <select name="idUser" id="idUser" required="true">
                            <% if (users != null) {
                                for (UsersEntity user : users) { %>
                            <option value="<%=user.getId()%>"  <%=user.getId() == idUser ? "selected" : ""%>><%=user.getName()%>
                            </option>
                            <% }
                            }%>
                        </select>
                    </div>
                    <div class="card_form__field">
                        <input type="submit" value="Готово">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="footer-menu">
        <div class="menu-column">
            <div class="column-head">
                Контакты
            </div>
            <div class="column-row">
                <div class="column-row__key">Адрес:</div>
                <div class="column-row__value">г. Москва Большая Тульская ул., 17</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">Телефон:</div>
                <div class="column-row__value">8 (495) 600-96-96</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">E-mail:</div>
                <div class="column-row__value">aldfas@mail.ru</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Сообщества
            </div>
            <div class="column-row">
                <div class="column-row__value">Facebook</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Twitter</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Instagram</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Справка
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="copyright-information">
            @2033 Yellow Duck Corporate. Все права защищены
        </div>
    </div>
</div>
</body>
</html>
