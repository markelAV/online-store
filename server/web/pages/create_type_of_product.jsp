<%@ page import="com.store.model.TypeProductEntity" %><%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 24.02.2020
  Time: 19:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    TypeProductEntity type = (TypeProductEntity) request.getAttribute("type");
    String name = "";
    String description = "";
    int id = -1;
    if (type != null) {
        name = type.getName();
        description = type.getDescription();
        id = type.getId();
    }
%>

<html>
<head>
    <title><%=id == -1 ? "Создание типа" : "Редактирование типа"%>
    </title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/create_card_style.css">
    <script src="script.js"></script>
</head>
<body>
<!-- The Modal -->
<div id="userMenu" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="popup-menu-user">
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Профиль</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/orders">Заказы</a>
            </div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/types">Типы
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/categories">Категории
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/products">Продукты</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/users">Пользователи</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/stores">Магазины</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Выход</a></div>
        </div>
    </div>

</div>
<div class="header">
    <div class="header">
        <div class="header-menu">
            <div class="menu-part-left">
                <div class="logo">
                    <img class="logo__img" lt="logo" src="images/logo.png"/>
                    <a class="logo__text" href="/server_war_exploded">
                        МузБар
                    </a>
                </div>
            </div>
            <div class="menu-part-middle"></div>
            <div class="menu-part-right">
                <div class="menu-items">
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/types-of-products">
                            Каталог
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/stores">
                            Магазины
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link">
                            О Нас
                        </a>
                    </div>
                </div>
                <div class="menu-profile">
                    <button class="profile__button" onclick="openMenu()"></button>
                    <div class="profile__name">
                        <a href="#" title="Dismissible popover" data-toggle="popover" data-placement="bottom"
                           data-trigger="focus"
                           data-content="<a class='menu-item__link' href='/server_war_exploded/orders'>Заказы</a>">Маркелов
                            А.</a>
                    </div>
                </div>
                <div class="shopping-card">
                    <a href="/server_war_exploded/shopping-card">
                        <img class="shopping-card__image"
                             src="images/shopping_card_icon.png"
                             alt="shopping_card"/></a>
                    <% String count = (String) request.getAttribute("countOrder");
                        if (count != null && !count.equals("") && Integer.parseInt(count) > 0) {%>
                    <div class="around-count">
                        <div class="around-count__text">
                            <%=count%>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    <div class="header-line">

    </div>
</div>
<div class="content">
    <div class="edit-form">
        <div class="enter_card">
            <div class="header_enter_card">
                <div class="header_enter_card__title"><%=id == -1 ? "Создание типа продукта" : "Редактирование типа"%>
                </div>
            </div>
            <div class="body_enter_card">
                <form class="card_form" method="post">
                    <intput id="id" name="id" value="<%=id == -1 ? "" : id%>>" type="text"
                            style="display: none"></intput>
                    <div class="card_form__field">
                        <label for="name">Имя</label>
                        <input id="name" name="name" type="text" value="<%=name%>">
                    </div>
                    <div class="card_form__field">
                        <label for="description">Описание</label>
                        <textarea id="description" name="description" type="text"
                                  value=""><%=description == null ? "" : description%></textarea>
                    </div>
                    <div class="card_form__field">
                        <input type="submit" value="Готово">
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="footer-menu">
        <div class="menu-column">
            <div class="column-head">
                Контакты
            </div>
            <div class="column-row">
                <div class="column-row__key">Адрес:</div>
                <div class="column-row__value">г. Москва Большая Тульская ул., 17</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">Телефон:</div>
                <div class="column-row__value">8 (495) 600-96-96</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">E-mail:</div>
                <div class="column-row__value">aldfas@mail.ru</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Сообщества
            </div>
            <div class="column-row">
                <div class="column-row__value">Facebook</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Twitter</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Instagram</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Справка
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="copyright-information">
            @2033 Yellow Duck Corporate. Все права защищены
        </div>
    </div>
</div>
</body>
</html>
