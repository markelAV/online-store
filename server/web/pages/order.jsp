<%@ page import="com.store.model.OrdersEntity" %>
<%@ page import="com.store.model.ProductsOfOrderEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="com.store.model.ProductEntity" %><%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 24.02.2020
  Time: 18:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Продукт</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/list_users_styles.css">
    <link rel="stylesheet" href="css/product_style.css">
    <script src="script.js"></script>
</head>
<body>
<!-- The Modal -->
<div id="userMenu" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="popup-menu-user">
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Профиль</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/orders">Заказы</a>
            </div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/types">Типы
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/categories">Категории
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/products">Продукты</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/users">Пользователи</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/stores">Магазины</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Выход</a></div>
        </div>
    </div>

</div>
<div class="header">
    <div class="header">
        <div class="header-menu">
            <div class="menu-part-left">
                <div class="logo">
                    <img class="logo__img" lt="logo" src="images/logo.png"/>
                    <a class="logo__text" href="/server_war_exploded">
                        МузБар
                    </a>
                </div>
            </div>
            <div class="menu-part-middle"></div>
            <div class="menu-part-right">
                <div class="menu-items">
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/types-of-products">
                            Каталог
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/stores">
                            Магазины
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link">
                            О Нас
                        </a>
                    </div>
                </div>
                <div class="menu-profile">
                    <button class="profile__button" onclick="openMenu()"></button>
                    <div class="profile__name">
                        <a href="#" title="Dismissible popover" data-toggle="popover" data-placement="bottom"
                           data-trigger="focus"
                           data-content="<a class='menu-item__link' href='/server_war_exploded/orders'>Заказы</a>">Маркелов
                            А.</a>
                    </div>
                </div>
                <div class="shopping-card">
                    <a href="/server_war_exploded/shopping-card">
                        <img class="shopping-card__image"
                             src="images/shopping_card_icon.png"
                             alt="shopping_card"/></a>
                    <% String count = (String) request.getAttribute("countOrder");
                        if (count != null && !count.equals("") && Integer.parseInt(count) > 0) {%>
                    <div class="around-count">
                        <div class="around-count__text">
                            <%=count%>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    <div class="header-line">

    </div>
</div>
<div class="content">
    <% OrdersEntity order = (OrdersEntity) request.getAttribute("order");
        if (order != null) {%>
    <div class="order_card_title">Заказ №<%=order.getId()%>
    </div>
    <div class="order_card">
        <div class="order_information">
            <div class="row-order">
                <div class="order_field_key">Статус</div>
                <div class="order_field_value"><%=order.getStatus()%>
                </div>
            </div>
            <div class="row-order">
                <div class="order_field_key">Сумма заказа</div>
                <div class="order_field_value"><%=order.getSum()%>
                </div>
            </div>
            <div class="row-order">
                <div class="order_field_key">Магазин доставки</div>
                <div class="order_field_value"><%=order.getStore().getAddress()%>
                </div>
            </div>
            <div class="row-order">
                <div class="order_field_key">Пользователь</div>
                <div class="order_field_value"><%=order.getUser().getName()%>
                </div>
            </div>
        </div>
        <div class="list_products">
            <table class="list_products__table">
                <th class="cell_name_key">Название товара</th>
                <th class="cell_count_key">Количество</th>
                <th class="cell_price_key">Цена</th>
                <% List<ProductsOfOrderEntity> productsOrder = (List<ProductsOfOrderEntity>) request.getAttribute("productsOrder");
                    List<ProductEntity> products = (List<ProductEntity>) request.getAttribute("products");
                    if (products != null && productsOrder != null) {
                        String nameProduct = "";
                        String priceProduct = "";
                        String countProduct = "";
                        String idProduct = "";
                        for (ProductsOfOrderEntity productOrder : productsOrder) {
                            countProduct = String.valueOf(productOrder.getCount());
                            for (ProductEntity product : products) {
                                if (product.getId() == productOrder.getProductIdOrder().getId_product()) {
                                    nameProduct = product.getName();
                                    priceProduct = String.valueOf(product.getPrice());
                                    idProduct = String.valueOf(product.getId());
                                }
                            }%>
                <tr class="table_row_product">
                    <td class="cell_name_value">
                        <a class="product_link"
                           href="/server_war_exploded/product?id=<%=idProduct%>">
                            <%=nameProduct%>
                        </a>
                    </td>
                    <td class="cell_count_value"><%=countProduct%>
                    </td>
                    <td class="cell_price_value"><%=priceProduct%>
                    </td>
                </tr>
                <% }
                }%>
            </table>
        </div>
    </div>
    <%} else {%>
    <div class="error_page_message">
        <div class="error_page_message__text">
            Заказ не найден. Пожалуйста обратитесь к Менеджеру.
        </div>
    </div>
    <%}%>
</div>
<div class="footer">
    <div class="footer-menu">
        <div class="menu-column">
            <div class="column-head">
                Контакты
            </div>
            <div class="column-row">
                <div class="column-row__key">Адрес:</div>
                <div class="column-row__value">г. Москва Большая Тульская ул., 17</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">Телефон:</div>
                <div class="column-row__value">8 (495) 600-96-96</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">E-mail:</div>
                <div class="column-row__value">aldfas@mail.ru</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Сообщества
            </div>
            <div class="column-row">
                <div class="column-row__value">Facebook</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Twitter</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Instagram</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Справка
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="copyright-information">
            @2033 Yellow Duck Corporate. Все права защищены
        </div>
    </div>
</div>
</body>
</html>
