<%@ page import="com.store.model.CategoryProductEntity" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 22.02.2020
  Time: 0:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>categories</h1>
<ul>
    <% List<CategoryProductEntity> types = (List<CategoryProductEntity>) request.getAttribute("categories");
        if (types != null) {
            for (CategoryProductEntity type : types) { %>
    <li>
        <a href="/server_war_exploded/products?id=<%=type.getId()%>"><%=type.getName()%>
        </a>
    </li>
    <% }
    }%>
</ul>

</body>
</html>
