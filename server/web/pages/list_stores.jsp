<%@ page import="com.store.model.StoreEntity" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 26.02.2020
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Магазины</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/list_users_styles.css">
    <script src="script.js"></script>
</head>
<body>
<!-- The Modal -->
<div id="userMenu" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="popup-menu-user">
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Профиль</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/orders">Заказы</a>
            </div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/types">Типы
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="/server_war_exploded/categories">Категории
                продуктов</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/products">Продукты</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/users">Пользователи</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link"
                                                  href="/server_war_exploded/stores">Магазины</a></div>
            <div class="popup-menu-user__item"><a class="menu-item__link" href="">Выход</a></div>
        </div>
    </div>

</div>
<div class="header">
    <div class="header">
        <div class="header-menu">
            <div class="menu-part-left">
                <div class="logo">
                    <img class="logo__img" lt="logo" src="images/logo.png"/>
                    <a class="logo__text" href="/server_war_exploded">
                        МузБар
                    </a>
                </div>
            </div>
            <div class="menu-part-middle"></div>
            <div class="menu-part-right">
                <div class="menu-items">
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/types-of-products">
                            Каталог
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link" href="/server_war_exploded/stores">
                            Магазины
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-item__link">
                            О Нас
                        </a>
                    </div>
                </div>
                <div class="menu-profile">
                    <button class="profile__button" onclick="openMenu()"></button>
                    <div class="profile__name">
                        <a href="#" title="Dismissible popover" data-toggle="popover" data-placement="bottom"
                           data-trigger="focus"
                           data-content="<a class='menu-item__link' href='/server_war_exploded/orders'>Заказы</a>">Маркелов
                            А.</a>
                    </div>
                </div>
                <div class="shopping-card">
                    <a href="/server_war_exploded/shopping-card">
                        <img class="shopping-card__image"
                             src="images/shopping_card_icon.png"
                             alt="shopping_card"/></a>
                    <% String count = (String) request.getAttribute("countOrder");
                        if (count != null && !count.equals("") && Integer.parseInt(count) > 0) {%>
                    <div class="around-count">
                        <div class="around-count__text">
                            <%=count%>
                        </div>
                    </div>
                    <%}%>
                </div>
            </div>
        </div>
    </div>
    <div class="header-line">

    </div>
</div>
<div class="content">
    <div class="content-header">
        <div class="content-header-information">
            <div class="title-page">
                Магазины
            </div>
            <div class="header-controls">
                <div class="controls-search">
                    <input id="input_search" type="text" placeholder="Найти">
                </div>
                <div class="controls-filter">
                    <select>
                        <option value="1">По ролям</option>
                    </select>
                </div>
                <div class="controls-filter">
                    <a href="/server_war_exploded/add-store" class="controls-add">Добавить магазин</a>
                </div>
            </div>
        </div>
        <div class="content-header-line">
        </div>
    </div>
    <div class="content-body">
        <% List<StoreEntity> stores = (List<StoreEntity>) request.getAttribute("stores");
            if (stores != null) {
                for (StoreEntity store : stores) { %>
        <div class="user_card">
            <div class="block_icon">

                <img class="block_icon__image" alt="type_avatar"
                     src="images/user_icon.png"/>
            </div>
            <div class="block_information">
                <div class="information_part1">
                    <div class="information_key">Адрес</div>
                    <a class="information_value" href="/server_war_exploded/store?id=<%=store.getId()%>">
                        <%=store.getAddress()%>
                    </a>
                </div>
                <div class="information_part2">
                    <div class="information_key">Часы работы</div>
                    <div class="information_value">
                        <%=store.getWorkingHours()%>
                    </div>
                </div>
            </div>
            <div class="block-controls">
                <div class="block-controls__item">
                    <a href="/server_war_exploded/add-store?id=<%=store.getId()%>">
                        <img class="controls-item__image" alt="edit" src="images/edit.png"/>
                    </a>
                </div>
                <div class="block-controls__item">
                    <a href="/server_war_exploded/delete?entity=store&id=<%=store.getId()%>">
                        <img class="controls-item__image" alt="remove" src="images/basket.png"/>
                    </a>
                </div>
            </div>
        </div>
        <% }
        }%>
    </div>
</div>
<div class="footer">
    <div class="footer-menu">
        <div class="menu-column">
            <div class="column-head">
                Контакты
            </div>
            <div class="column-row">
                <div class="column-row__key">Адрес:</div>
                <div class="column-row__value">г. Москва Большая Тульская ул., 17</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">Телефон:</div>
                <div class="column-row__value">8 (495) 600-96-96</div>
            </div>
            <div class="column-row">
                <div class="column-row__key">E-mail:</div>
                <div class="column-row__value">aldfas@mail.ru</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Сообщества
            </div>
            <div class="column-row">
                <div class="column-row__value">Facebook</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Twitter</div>
            </div>
            <div class="column-row">
                <div class="column-row__value">Instagram</div>
            </div>
        </div>
        <div class="menu-column">
            <div class="column-head">
                Справка
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="copyright-information">
            @2033 Yellow Duck Corporate. Все права защищены
        </div>
    </div>
</div>
</body>
</html>
