<%@ page import="com.store.model.TypeProductEntity" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 08.02.2020
  Time: 23:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Catalog</title>
</head>
<body>
<h1>Catalog</h1>
<div>
    <ul>
        <% List<TypeProductEntity> types = (List<TypeProductEntity>) request.getAttribute("types");
            if (types != null) {
                for (TypeProductEntity type : types) { %>
        <li>
            <a href="/server_war_exploded/category?id=<%=type.getId()%>"><%=type.getName()%>
            </a>
        </li>
        <% }
        }%>
    </ul>
</div>
</body>
</html>
